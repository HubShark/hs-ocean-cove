HubShark - codename OceanCove
=============================

OceanCove is based on the Symfony3 Demo.

## Planned Main Changes

* Removing the built-in Admin suport. using EasyAdmin as a replacement.
* Removing the built-in  User suport. using FOSUserBundle as a replacement
* Prettify the System
* Make EU Cookie Law Compliant
* Add a Versioning System

## How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/HubShark/hs-ocean-cove.git hs-ocean-cove`

IMPORTANT!!! Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd hs-ocean-cove`

Then fix the permissions like this:

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```

The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an Online Generated key here.

`composer install`

answer any questions and it will or may end with errors message about an unkown databank. Fix that error with this:

`php bin/console doctrine:database:create`

Then Create the database schema

`php bin/console doctrine:schema:create`

And now update your database:

`php bin/console doctrine:schema:update --force`

Then create your :Admin User_ (Replace "**Admin**" with your adminuser name):

`php bin/console fos:user:create Admin --super-admin`

And then we run this to update everything:

`composer update --with-dependencies`


Then lod the fixture to see the content:

`php bin/console doctrine:fixtures:load`



-----------------------------------------





||| ============================================= |||<br>
||| ======== _**BELOW IS THE ORIGANL README**_ ======== |||<br>
||| ============================================= |||<br>





------------------------------------------

## Symfony Demo Application

The "Symfony Demo Application" is a reference application created to show how
to develop Symfony applications following the recommended best practices.

[![Build Status](https://travis-ci.org/symfony/symfony-demo.svg?branch=master)](https://travis-ci.org/symfony/symfony-demo)

Requirements
------------

  * PHP 5.5.9 or higher;
  * PDO-SQLite PHP extension enabled;
  * and the [usual Symfony application requirements](http://symfony.com/doc/current/reference/requirements.html).

If unsure about meeting these requirements, download the demo application and
browse the `http://localhost:8000/config.php` script to get more detailed
information.

Installation
------------

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

First, install the [Symfony Installer](https://github.com/symfony/symfony-installer)
if you haven't already. Then, install the Symfony Demo Application executing
this command anywhere in your system:

```bash
$ symfony demo

# if you're using Windows:
$ php symfony demo
```

If the `demo` command is not available, update your Symfony Installer to the
most recent version executing the `symfony self-update` command.

> **NOTE**
>
> If you can't use the Symfony Installer, download and install the demo
> application using Git and Composer:
>
>     $ git clone https://github.com/symfony/symfony-demo
>     $ cd symfony-demo/
>     $ composer install --no-interaction

Usage
-----

There is no need to configure a virtual host in your web server to access the application.
Just use the built-in web server:

```bash
$ cd symfony-demo/
$ php bin/console server:run
```

This command will start a web server for the Symfony application. Now you can
access the application in your browser at <http://localhost:8000>. You can
stop the built-in web server by pressing `Ctrl + C` while you're in the
terminal.

> **NOTE**
>
> If you want to use a fully-featured web server (like Nginx or Apache) to run
> Symfony Demo application, configure it to point at the `web/` directory of the project.
> For more details, see:
> http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html

Troubleshooting
---------------

The current Symfony Demo application uses Symfony 3.x version. If you want to
use the legacy Symfony 2.8 version, clone the Git repository and checkout the
`v0.8.4` tag, which is the last one compatible with Symfony 2.8:

```bash
$ git clone https://github.com/symfony/symfony-demo
$ cd symfony-demo/
$ git checkout tags/v0.8.4 -b v0.8.4
$ composer install
```
